# Custom Test Cases for AUD UE 7

## Gemischte Brüche

````java
    @Test
	public void konstruktor_customTest(){
		long a = -3;
		long b = -15;
		long c = -28;
		GemischterBruch bruch = new GemischterBruch(a, b, c);
		assertTrue("Der gemischte Bruch muss negativ sein", !bruch.istPositiv());
	}
````
	
````java
	@Test
	public void addiereZu_customTest(){
		AbstrakterGemischterBruch[] summanden_1 = {new GemischterBruch(0,0,1) , new GemischterBruch(-17,-2,-1) , new GemischterBruch(3,7,8)};
		AbstrakterGemischterBruch[] summanden_2 = {new GemischterBruch(0,0,1) , new GemischterBruch(16,3*45,45) , new GemischterBruch(25,1,9)};
		
		boolean[] expectedPositiv = {true , true, true};
		long[] expectedGanzzahl = {0 , 0, 28};
		long[] expectedZaehler = {0 , 0, 71};
		
		for(int i=0 ; i<summanden_1.length ; i++){
			AbstrakterGemischterBruch sum1 = summanden_1[i];
			AbstrakterGemischterBruch sum2 = summanden_2[i];
			
			AbstrakterGemischterBruch erg = sum1.addiereZu(sum2);
			
			assertEquals("Falsches Vorzeichen: "+sum1.toString()+" + "+sum2.toString() , expectedPositiv[i] , erg.istPositiv());
			assertEquals("Falscher ganzzahliger Anteil:"+sum1.toString()+" + "+sum2.toString() , expectedGanzzahl[i] , erg.holeGanzzahligerAnteil());
			assertEquals("Falscher Zaehler:"+sum1.toString()+" + "+sum2.toString() , expectedZaehler[i] , erg.holeZaehler());
		}
	}
````
	
````java
	@Test
	public void subtrahieren_customTest(){
		AbstrakterGemischterBruch[] minuend = {new GemischterBruch(0,0,1) , new GemischterBruch(-17,-2,-1) , new GemischterBruch(3556,893,444)};
		AbstrakterGemischterBruch[] subtrahend = {new GemischterBruch(0,0,1) , new GemischterBruch(16,3*45,45) , new GemischterBruch(12,13,-25)};
		
		boolean[] expectedPositiv = {true , false, true};
		long[] expectedGanzzahl = {0 , 38, 3570};
		long[] expectedZaehler = {0 , 0, 5897};
		
		for(int i=0 ; i<minuend.length ; i++){
			AbstrakterGemischterBruch min = minuend[i];
			AbstrakterGemischterBruch sub = subtrahend[i];
			
			AbstrakterGemischterBruch erg = min.subtrahiereDavon(sub);
			
			assertEquals("Falsches Vorzeichen: "+min.toString()+" + "+sub.toString() , expectedPositiv[i] , erg.istPositiv());
			assertEquals("Falscher ganzzahliger Anteil:"+min.toString()+" + "+sub.toString() , expectedGanzzahl[i] , erg.holeGanzzahligerAnteil());
			assertEquals("Falscher Zaehler:"+min.toString()+" + "+sub.toString() , expectedZaehler[i] , erg.holeZaehler());
		}
	}
````

````java
@Test(timeout = 666)
	public void customLongTest__addiereZu() {
		AbstrakterGemischterBruch[][] inOut = { //
				{ new GemischterBruch(2, 3, 5), new GemischterBruch(7, 1, 10) }, //
				{ new GemischterBruch(2, 3, 4), new GemischterBruch(7, 1, 2) }, //
				{ new GemischterBruch(2, 17, 42), new GemischterBruch(-3, 19, 42) }, //

				{ new GemischterBruch(-2, 17, 42), new GemischterBruch(2, 17, 42) }, //
				{ new GemischterBruch(2, -17, 42), new GemischterBruch(2, 17, 42) }, //
				{ new GemischterBruch(2, 17, -42), new GemischterBruch(2, 17, 42) }, //
				{ new GemischterBruch(2, 17, 42), new GemischterBruch(-2, 17, 42) }, //
				{ new GemischterBruch(2, 17, 42), new GemischterBruch(2, -17, 42) }, //
				{ new GemischterBruch(2, 17, 42), new GemischterBruch(2, 17, -42) }, //

				{ new GemischterBruch(-2, 17, 42), new GemischterBruch(-2, 17, 42) }, //
				{ new GemischterBruch(2, -17, 42), new GemischterBruch(2, -17, 42) }, //
				{ new GemischterBruch(2, 17, -42), new GemischterBruch(2, 17, -42) }, //
				{ new GemischterBruch(2, 17, -42), new GemischterBruch(-2, 17, 42) }, //
				{ new GemischterBruch(2, -17, 42), new GemischterBruch(2, -17, 42) }, //
				{ new GemischterBruch(-2, 17, 42), new GemischterBruch(2, 17, -42) }, //
		};
		long[][] expected = { //
				{ 9, 7, 10 }, //
				{ 10, 1, 4 }, //
				{ 1, 1, 21 }, //

				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //

				{ 4, 17, 21 }, //
				{ 4, 17, 21 }, //
				{ 4, 17, 21 }, //
				{ 4, 17, 21 }, //
				{ 4, 17, 21 }, //
				{ 4, 17, 21 }, //
				
				
		};
		boolean[] istPositiv = { //
				true, //
				true, //
				false, //
				
				true,
				true,
				true,				
				true,
				true,
				true,
				
				false,
				false,
				false,
				false,
				false,
				false,
				
		};
		for (int i = 0; i < inOut.length; i++) {
			AbstrakterGemischterBruch b1 = inOut[i][0];
			AbstrakterGemischterBruch b2 = inOut[i][1];
			AbstrakterGemischterBruch b1_add_b2 = b1.addiereZu(b2);
			assertEquals(b1 + "+" + b2 + "=?=" + b1_add_b2 + " @ " + GemischterBruchPublicTest.METHOD_NAME_istPositiv + " failed.", istPositiv[i], b1_add_b2.istPositiv());
			assertEquals(b1 + "+" + b2 + "=?=" + b1_add_b2 + " @ " + GemischterBruchPublicTest.METHOD_NAME_holeGanzzahligerAnteil + " failed.", expected[i][0], b1_add_b2.holeGanzzahligerAnteil());
			assertEquals(b1 + "+" + b2 + "=?=" + b1_add_b2 + " @ " + GemischterBruchPublicTest.METHOD_NAME_holeZaehler + " failed.", expected[i][1], b1_add_b2.holeZaehler());
			assertEquals(b1 + "+" + b2 + "=?=" + b1_add_b2 + " @ " + GemischterBruchPublicTest.METHOD_NAME_holeNenner + " failed.", expected[i][2], b1_add_b2.holeNenner());
		}
	}

````

````java
@Test(timeout = 666)
	public void customLongTest__subtrahiereDavon() {
		AbstrakterGemischterBruch[][] inOut = { //
				{ new GemischterBruch(0, 1, 1), new GemischterBruch(0, 1, 1) }, //
				{ new GemischterBruch(0, 1, 1234234), new GemischterBruch(0, 1, 1234234) }, //
				
				{ new GemischterBruch(-2, 1, 5), new GemischterBruch(-2, 1, 5) }, //
				{ new GemischterBruch(-2, 1, 5), new GemischterBruch(2, -1, 5) }, //
				{ new GemischterBruch(-2, 1, 5), new GemischterBruch(2, 1, -5) }, //
				{ new GemischterBruch(2, -1, 5), new GemischterBruch(-2, 1, 5) }, //
				{ new GemischterBruch(2, -1, 5), new GemischterBruch(2, -1, 5) }, //
				{ new GemischterBruch(2, -1, 5), new GemischterBruch(2, 1, -5) }, //
				{ new GemischterBruch(2, 1, -5), new GemischterBruch(-2, 1, 5) }, //
				{ new GemischterBruch(2, 1, -5), new GemischterBruch(2, -1, 5) }, //
				{ new GemischterBruch(2, 1, -5), new GemischterBruch(2, 1, -5) }, //
				
				{ new GemischterBruch(-3, 1, 6), new GemischterBruch(-2, 1, 5) }, //
				{ new GemischterBruch(-3, 1, 6), new GemischterBruch(2, -1, 5) }, //
				{ new GemischterBruch(-3, 1, 6), new GemischterBruch(2, 1, -5) }, //
				{ new GemischterBruch(3, -1, 6), new GemischterBruch(-2, 1, 5) }, //
				{ new GemischterBruch(3, -1, 6), new GemischterBruch(2, -1, 5) }, //
				{ new GemischterBruch(3, -1, 6), new GemischterBruch(2, 1, -5) }, //
				{ new GemischterBruch(3, 1, -6), new GemischterBruch(-2, 1, 5) }, //
				{ new GemischterBruch(3, 1, -6), new GemischterBruch(2, -1, 5) }, //
				{ new GemischterBruch(3, 1, -6), new GemischterBruch(2, 1, -5) }, //
				
				{ new GemischterBruch(2, 1, 5), new GemischterBruch(-2, 1, 5) }, //
				{ new GemischterBruch(2, 1, 5), new GemischterBruch(2, -1, 5) }, //
				{ new GemischterBruch(2, 1, 5), new GemischterBruch(2, 1, -5) }, //
				
				
				{ new GemischterBruch(2, 3, 5), new GemischterBruch(7, 1, 10) }, //
				{ new GemischterBruch(2, 3, 4), new GemischterBruch(7, 1, 2) }, //
				{ new GemischterBruch(2, 17, 42), new GemischterBruch(-3, 19, 42) }, //
		};
		long[][] expected = { //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				{ 0, 0, 1 }, //
				
				{ 0, 29, 30 }, //
				{ 0, 29, 30 }, //
				{ 0, 29, 30 }, //
				{ 0, 29, 30 }, //
				{ 0, 29, 30 }, //
				{ 0, 29, 30 }, //
				{ 0, 29, 30 }, //
				{ 0, 29, 30 }, //
				{ 0, 29, 30 }, //
				
				{ 4, 2, 5 }, //
				{ 4, 2, 5 }, //
				{ 4, 2, 5 }, //

				{ 4, 1, 2 }, //
				{ 4, 3, 4 }, //
				{ 5, 6, 7 }, //
		};
		boolean[] istPositiv = { //
				true, //
				true, //

				true, //
				true, //
				true, //
				true, //
				true, //
				true, //
				true, //
				true, //
				true, //
				
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				
				true, //
				true, //
				true, //
				
				false, //
				false, //
				true, //
		};
		for (int i = 0; i < inOut.length; i++) {
			AbstrakterGemischterBruch b1 = inOut[i][0];
			AbstrakterGemischterBruch b2 = inOut[i][1];
			AbstrakterGemischterBruch b1_sub_b2 = b1.subtrahiereDavon(b2);
			assertEquals(b1 + "-" + b2 + "=?=" + b1_sub_b2 + " @ " + GemischterBruchPublicTest.METHOD_NAME_istPositiv + " failed.", istPositiv[i], b1_sub_b2.istPositiv());
			assertEquals(b1 + "-" + b2 + "=?=" + b1_sub_b2 + " @ " + GemischterBruchPublicTest.METHOD_NAME_holeGanzzahligerAnteil + " failed.", expected[i][0], b1_sub_b2.holeGanzzahligerAnteil());
			assertEquals(b1 + "-" + b2 + "=?=" + b1_sub_b2 + " @ " + GemischterBruchPublicTest.METHOD_NAME_holeZaehler + " failed.", expected[i][1], b1_sub_b2.holeZaehler());
			assertEquals(b1 + "-" + b2 + "=?=" + b1_sub_b2 + " @ " + GemischterBruchPublicTest.METHOD_NAME_holeNenner + " failed.", expected[i][2], b1_sub_b2.holeNenner());
		}
	}

````

````java
	@Test(timeout = 666)
	public void custom__compareTo() {
		assertEquals(new GemischterBruch(-2, 17, 42).compareTo(new GemischterBruch(-2, 17, 42)),0);
		assertEquals(new GemischterBruch(2, -17, 42).compareTo(new GemischterBruch(-2, 17, 42)),0);
		assertEquals(new GemischterBruch(2, 17, -42).compareTo(new GemischterBruch(-2, 17, 42)),0);
		assertEquals(new GemischterBruch(0, -101, 42).compareTo(new GemischterBruch(-2, 17, 42)),0);
		assertEquals(new GemischterBruch(0, -101, 42).compareTo(new GemischterBruch(-2, 17, 42)),0);
		assertEquals(new GemischterBruch(0, 101, -42).compareTo(new GemischterBruch(-2, 17, 42)),0);
		
		assertEquals(new GemischterBruch(-3, 17, 42).compareTo(new GemischterBruch(-2, 17, 42)),-1);
		assertEquals(new GemischterBruch(3, -17, 42).compareTo(new GemischterBruch(-2, 17, 42)),-1);
		assertEquals(new GemischterBruch(3, 17, -42).compareTo(new GemischterBruch(-2, 17, 42)),-1);
		assertEquals(new GemischterBruch(1, -101, 42).compareTo(new GemischterBruch(-2, 17, 42)),-1);
		assertEquals(new GemischterBruch(1, -101, 42).compareTo(new GemischterBruch(-2, 17, 42)),-1);
		assertEquals(new GemischterBruch(1, 101, -42).compareTo(new GemischterBruch(-2, 17, 42)),-1);

		assertEquals(new GemischterBruch(-3, 17, 42).compareTo(new GemischterBruch(-4, 17, 42)),1);
		assertEquals(new GemischterBruch(3, -17, 42).compareTo(new GemischterBruch(-4, 17, 42)),1);
		assertEquals(new GemischterBruch(3, 17, -42).compareTo(new GemischterBruch(-4, 17, 42)),1);
		assertEquals(new GemischterBruch(1, -101, 42).compareTo(new GemischterBruch(-4, 17, 42)),1);
		assertEquals(new GemischterBruch(1, -101, 42).compareTo(new GemischterBruch(-4, 17, 42)),1);
		assertEquals(new GemischterBruch(1, 101, -42).compareTo(new GemischterBruch(-4, 17, 42)),1);
	}
````


````java
    @Test(timeout = 666)
    public void custom_ConstructorTest() {
        GemischterBruch[] gemischterBruch = new GemischterBruch[42];

        for(int pass = 0; pass < 42; ++pass) {
            int randomInt = ThreadLocalRandom.current().nextInt(-100, 100);
            int randomUp = ThreadLocalRandom.current().nextInt(-100, 100);
            int randomLow = ThreadLocalRandom.current().nextInt(-100, 100);

            if(randomLow == 0) {
                continue;
            }
            gemischterBruch[pass] = new GemischterBruch(randomInt,randomUp, randomLow );

            boolean isPositive = true;
            if(randomInt < 0) {
                isPositive = !isPositive;
            }

            if(randomUp < 0) {
                isPositive = !isPositive;
            }

            if(randomLow < 0) {
                isPositive = !isPositive;
            }

            assertEquals("[" + pass + "] Int: " + randomInt + "; Up: " + randomUp + "; Low: " + randomLow, isPositive, gemischterBruch[pass].istPositiv());
         }
    }
````