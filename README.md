# Custom Test Cases for AUD

### Übungsblätter:
* [Übungsblatt 5](UE5.md)
* [Übungsblatt 6](UE6.md)
* [Übungsblatt 7](UE7.md)
* [Übungsblatt 8](UE8.md)
* [Übungsblatt 9](UE9.md)
* [Übungsblatt Bonus](Bonus.md)
* [Übungsblatt 10](UE10.md)
* [Übungsblatt 11](UE11.md)
