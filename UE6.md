# Custom Test Cases for AUD UE 6

## Example Excersise Custom Tests
````java
@Test(timeout = 666)
public void customTestExcersise() {
	String[] actual = excersise.generate("test");
	assertEquals("Unexpected length of returned Gray code array, should 0", 0, actual.length);
}
````