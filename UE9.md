# Custom Test Cases for AUD UE 9

## ZirkulaereDoppeltVerkettetePrioritaetswarteschlange
````java
@Test(timeout = 666)
public void custTest_All() {
    ZirkulaereDoppeltVerkettetePrioritaetswarteschlange<String> zdvpws = new ZirkulaereDoppeltVerkettetePrioritaetswarteschlange<>(COMP_STRING_LEX);
    zdvpws.einfuegen("B");
    zdvpws.einfuegen("1");
    zdvpws.einfuegen("2");
    zdvpws.einfuegen("3");
    zdvpws.einfuegen("A");
    zdvpws.einfuegen("A");
    zdvpws.einfuegen("A");
    zdvpws.einfuegen("C");
    zdvpws.einfuegen("E");
    zdvpws.einfuegen("D");
    zdvpws.einfuegen("4");
    zdvpws.einfuegen("7");
    zdvpws.einfuegen("6");
    zdvpws.einfuegen("5");
    zdvpws.einfuegen("F");
    assertEquals("Failed.","#> F > E > D > C > B > A > A > A > 7 > 6 > 5 > 4 > 3 > 2 > 1 >|",zdvpws.toString());
    assertEquals("Failed.","F",zdvpws.kopf.holeWert());
    assertEquals("Failed.","F",zdvpws.wichtigstenEntfernen());
    assertEquals("Failed.","#> E > D > C > B > A > A > A > 7 > 6 > 5 > 4 > 3 > 2 > 1 >|",zdvpws.toString());
    assertEquals("Failed.","1",zdvpws.wichtigstenEntfernen(COMP_STRING_ANTI_LEX));
    assertEquals("Failed.","E",zdvpws.unwichtigstenEntfernen(COMP_STRING_ANTI_LEX));
    assertEquals("Failed.","2",zdvpws.unwichtigstenEntfernen());
    assertEquals("Failed.","D",zdvpws.wichtigstenEntfernen());
    assertEquals("Failed.","3",zdvpws.unwichtigstenEntfernen());
    assertEquals("Failed.","#> C > B > A > A > A > 7 > 6 > 5 > 4 >|",zdvpws.toString());
    assertEquals("Failed.","C",zdvpws.unwichtigstenEntfernen(COMP_STRING_ANTI_LEX));
    assertEquals("Failed.","B",zdvpws.wichtigstenEntfernen());
    assertEquals("Failed.","A",zdvpws.wichtigstenEntfernen());
    assertEquals("Failed.","A",zdvpws.unwichtigstenEntfernen(COMP_STRING_ANTI_LEX));
    assertEquals("Failed.","A",zdvpws.wichtigstenEntfernen());
    assertEquals("Failed.","4",zdvpws.unwichtigstenEntfernen());
    assertEquals("Failed.","5",zdvpws.wichtigstenEntfernen(COMP_STRING_ANTI_LEX));
    assertEquals("Failed.","6",zdvpws.unwichtigstenEntfernen());
    assertEquals("Failed.","#> 7 >|",zdvpws.toString());
    assertEquals("Failed.","7",zdvpws.wichtigstenEntfernen());
    assertEquals("Failed.","#>|",zdvpws.toString());
    try {
    		assertEquals("Failed.",null,zdvpws.wichtigstenEntfernen());
    } catch (Exception e) {
    		if(e instanceof NoSuchElementException) {
    		    assertEquals("Failed.","#>|",zdvpws.toString());
    		} else {
    			fail("Failed. Expected NoSuchElementException, but was: "+e);
    		}
    }
    
    try {
			assertEquals("Failed.",null,zdvpws.unwichtigstenEntfernen());
    } catch (Exception e) {
    		if(e instanceof NoSuchElementException) {
    		    assertEquals("Failed.","#>|",zdvpws.toString());
    		} else {
    			fail("Failed. Expected NoSuchElementException, but was: "+e);
    		}
    }
    
    try {
			assertEquals("Failed.",null,zdvpws.wichtigstenEntfernen(COMP_STRING_ANTI_LEX));
    } catch (Exception e) {
    		if(e instanceof NoSuchElementException) {
    		    assertEquals("Failed.","#>|",zdvpws.toString());
    		} else {
    			fail("Failed. Expected NoSuchElementException, but was: "+e);
    		}
    }
    
    try {
			assertEquals("Failed.",null,zdvpws.unwichtigstenEntfernen(COMP_STRING_ANTI_LEX));
    } catch (Exception e) {
    		if(e instanceof NoSuchElementException) {
    		    assertEquals("Failed.","#>|",zdvpws.toString());
    		} else {
    			fail("Failed. Expected NoSuchElementException, but was: "+e);
    		}
    }
}
````

## 9.1 ADT-I
````java

@Test(timeout = 666)
public void customTest_nat2int_zero() {
	Int actual = Int.nat2int(Sign.plus(), zero);
	assertSame("You failed: +0 == (*+*0)", Sign.plus(), Int.sign(actual));
	IntPublicTest.assertSameNat("You failed: +0 == (+*0*)", zero, Int.nat(actual));
}

@Test(timeout = 666)
public void customTest_nat2int_negative() {
	Int actual = Int.nat2int(Sign.minus(), n42);
	assertSame("You failed: -42 == (*-*42)", Sign.minus(), Int.sign(actual));
	IntPublicTest.assertSameNat("You failed: -42 == (-*42*)", n42, Int.nat(actual));
}

@Test(timeout = 666)
public void customTest_nat2int_zero_Sign_minus() {
	Int actual = Int.nat2int(Sign.minus(), zero);
	assertSame("Fehler 'zero' muss immer ein positives Vorzeichen haben!", Sign.plus(), Int.sign(actual));
}

@Test(timeout = 666)
public void customTest_uminus_zero() {
	Int actual = Int.uminus(Int.nat2int(Sign.plus(), zero));
	assertSame("Fehler 'zero' muss immer ein positives Vorzeichen haben!", Sign.plus(), Int.sign(actual));
}

@Test(timeout = 666)
public void customTest_add_zero() {
	Int a = Int.nat2int(Sign.plus(), four);
	Int b = Int.nat2int(Sign.minus(), four);
	Int actual = Int.add(a, b);
	assertSame("Fehler 'zero' muss immer ein positives Vorzeichen haben!", Sign.plus(), Int.sign(actual));
	
	actual = Int.add(b, a);
	assertSame("Fehler 'zero' muss immer ein positives Vorzeichen haben!", Sign.plus(), Int.sign(actual));
}

@Test(timeout = 666)
public void customTest_sub_zero() {
	Int a = Int.nat2int(Sign.plus(), four);
	Int b = Int.nat2int(Sign.minus(), four);
	Int actual = Int.sub(a, a);
	assertSame("Fehler 'zero' muss immer ein positives Vorzeichen haben!", Sign.plus(), Int.sign(actual));

	actual = Int.sub(b, b);
	assertSame("Fehler 'zero' muss immer ein positives Vorzeichen haben!", Sign.plus(), Int.sign(actual));
}

@Test(timeout = 666)
public void customTest_mul_zero() {
	Int a = Int.nat2int(Sign.plus(), four);
	Int b = Int.nat2int(Sign.plus(), zero);
	Int actual = Int.mul(a, b);
	assertSame("Fehler: 'zero' muss immer ein positives Vorzeichen haben!", Sign.plus(), Int.sign(actual));
	assertSame("Fehler: Nat * 0 ist immer 0 !", Nat.zero(), Int.nat(actual));
	
	actual = Int.mul(b, a);
	assertSame("Fehler 'zero' muss immer ein positives Vorzeichen haben!", Sign.plus(), Int.sign(actual));
	assertSame("Fehler: 0 * Nat ist immer 0 !", Nat.zero(), Int.nat(actual));
}

@Test(timeout = 666)
public void customTest_div_zero() {
	Int a = Int.nat2int(Sign.plus(), four);
	Int b = Int.nat2int(Sign.plus(), zero);
	Int actual = Int.div(b, a);
	assertSame("Fehler: 'zero' muss immer ein positives Vorzeichen haben!", Sign.plus(), Int.sign(actual));
	assertSame("Fehler: 0 : Nat ist immer 0 !", Nat.zero(), Int.nat(actual));
	
}

@Test(expected = StackOverflowError.class)
public void customTest_div_by_zero() {
	Int a = Int.nat2int(Sign.plus(), four);
	Int b = Int.nat2int(Sign.plus(), zero);
	
	Int.div(a, b);
}
````