# Custom Test Cases for AUD UE 10

## 10.4: SkipList
### Checks the use of NullPointerExceptions in the affected methods
````java
@Test(expected=NullPointerException.class)
public void customTest_addAll_NullPointerExceptions() {
	AbstractSkipList<Integer> sl = new SkipList<>();
	sl.addAll(null);
}
@Test(expected=NullPointerException.class)
public void customTest_contains_NullPointerExceptions() {
	AbstractSkipList<Integer> sl = new SkipList<>();
	sl.contains(null);
}
@Test(expected=NullPointerException.class)
public void customTest_containsAll_NullPointerExceptions() {
	AbstractSkipList<Integer> sl = new SkipList<>();
	sl.containsAll(null);
}
@Test(expected=NullPointerException.class)
public void customTest_remove_NullPointerExceptions() {
	AbstractSkipList<Integer> sl = new SkipList<>();
	sl.remove(null);
}
@Test(expected=NullPointerException.class)
public void customTest_removeAll_NullPointerExceptions() {
	AbstractSkipList<Integer> sl = new SkipList<>();
	sl.removeAll(null);
}
@Test(expected=NullPointerException.class)
public void customTest_add_NullPointerExceptions() {
	AbstractSkipList<Integer> sl = new SkipList<>();
	sl.add(null);
}
````