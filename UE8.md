# Custom Test Cases for AUD UE 8

## LightsOut
````java
    @Test
	public void solve2x1(){
		long state = 0b0__11;
		long mask = 0;
		LightsOut lightsOut = new LightsOut(2,1,state,mask);
		ZahlenFolgenMerker movesMerker = lightsOut.solve();
		Integer[] solutionActual = movesMerker == null ? null : movesMerker.gibtMirAlle();
		assertNotNull("solve2x1(): Solution NotNull failed",solutionActual);
		assertEquals("solve2x1(): ArrayLength failed | " , 1 , solutionActual.length);
		assertTrue("solve2x1(): AssertTrue failed | ",solutionActual[0] == 0);
	}
````

````java	
	@Test
	public void solve2x1_unsolvable(){
		long state = 0b0__10;
		long mask = 0;
		LightsOut lightsOut = new LightsOut(2,1,state,mask);
		ZahlenFolgenMerker movesMerker = lightsOut.solve();
		Integer[] solutionActual = movesMerker == null ? null : movesMerker.gibtMirAlle();
		assertNull("Spiel ist nicht loesbar",solutionActual);
	}
````
	
````java
	@Test
	public void solve2x2(){
		long state = 0b0__1110;
		long mask = 0;
		LightsOut lightsOut = new LightsOut(2,2,state,mask);
		ZahlenFolgenMerker movesMerker = lightsOut.solve();
		Integer[] solutionActual = movesMerker == null ? null : movesMerker.gibtMirAlle();
		assertNotNull("sove2x2(): Solution NotNull failed",solutionActual);
		assertEquals("sove2x2(): ArrayLength failed | " , 1 , solutionActual.length);
		assertTrue("sove2x2(): AssertTrue failed | ",solutionActual[0] == 3);
	}
````
	
````java	
	@Test(expected = IllegalArgumentException.class)
	public void sove8x9(){
		long state = 0b0__1110;
		long mask = 0;
		LightsOut lightsOut = new LightsOut(9,8,state,mask);
	}
````


## LightsOut Solve
````java

	@Test(timeout = 666)
	public void custSolve0() {
		  long state = 27874L;
		  long mask = 32773L;
		  long stateExpected = state;
		  LightsOut lightsOut = new LightsOut(4, 4, state, mask);
		  ZahlenFolgenMerker movesMerker = lightsOut.solve();
		  Integer[] solutionActual = movesMerker == null ? null : movesMerker.gibtMirAlle();
		  long stateActual = lightsOut.getState();
		  assertEquals("You toggled wrong.", stateExpected, stateActual);
		  assertNull("There is no solution, I know it!", solutionActual);

	}
	
	@Test(timeout = 666)
	public void custSolve1() {
		  long state = 27874L;
		  long mask = 32773L;
		  long stateExpected = state;
		  LightsOut lightsOut = new LightsOut(4, 4, state, mask);
		  ZahlenFolgenMerker movesMerker = lightsOut.solve();
		  Integer[] solutionActual = movesMerker == null ? null : movesMerker.gibtMirAlle();
		  long stateActual = lightsOut.getState();
		  assertEquals("You toggled wrong.", stateExpected, stateActual);
		  assertNull("There is no solution, I know it!", solutionActual);

	}
	
	@Test(timeout = 666)
	public void custSolve2() {
		  long state = 58360L;
		  long mask = 5L;
		  long stateExpected = state;
		  LightsOut lightsOut = new LightsOut(4, 4, state, mask);
		  ZahlenFolgenMerker movesMerker = lightsOut.solve();
		  Integer[] solutionActual = movesMerker == null ? null : movesMerker.gibtMirAlle();
		  long stateActual = lightsOut.getState();
		  assertEquals("You toggled wrong.", stateExpected, stateActual);
		  assertNull("There is no solution, I know it!", solutionActual);

	}
	
	@Test(timeout = 666)
	public void custSolve3() {
		  long state = 44002L;
		  long mask = Long.MAX_VALUE;
		  LightsOut lightsOut = new LightsOut(4, 4, state, mask);
		  ZahlenFolgenMerker movesMerker = lightsOut.solve();
		  Integer[] solutionActual = movesMerker == null ? null : movesMerker.gibtMirAlle();
		  assertEquals("0 toggles are enough here [].", 0, solutionActual.length);

	}
	
	@Test(timeout = 666)
	public void custSolve4() {
		  long state = 14893L;
		  long mask = 33984L;
		  long stateExpected = state;
		  LightsOut lightsOut = new LightsOut(4, 4, state, mask);
		  ZahlenFolgenMerker movesMerker = lightsOut.solve();
		  Integer[] solutionActual = movesMerker == null ? null : movesMerker.gibtMirAlle();
		  long stateActual = lightsOut.getState();
		  assertEquals("You toggled wrong.", stateExpected, stateActual);
		  assertEquals("7 toggles are enough here [4, 5, 9, 13, 11, 2, 14].", 7, solutionActual.length);
	}

````
