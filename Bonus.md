# Custom Test Cases for AUD Bonus UE

## DPLL SAT Solver
````java
// ============================== CUSTOM TEST ==============================
	@Test(timeout = 1000)
	public void custTest__solveFalse() {
		// (v0 | !v2) & (!v0 | !v1) & (!v0 | v2) & (v1 | v2)
		final List<List<Literal>> formula = new LinkedList<>();
		formula.add(mkClause(new Literal(0, false)));
		formula.add(mkClause(new Literal(0, true)));
		final Map<Integer, Boolean> assignments = new HashMap<>();
		final DPLLSolver solver = new DPLLSolver(10);
		boolean isSatisfiable = solver.solve(formula, assignments);
		assertFalse("failed",isSatisfiable);
	}
	
	@Test(timeout = 1000)
	public void custTest__solveBig() {
		// (v0 | !v2) & (!v0 | !v1) & (!v0 | v2) & (v1 | v2)
		final List<List<Literal>> formula = new LinkedList<>();
		formula.add(mkClause(new Literal(0, false)));
		formula.add(mkClause(new Literal(0, false), new Literal(0, true)));
		formula.add(mkClause(new Literal(2, false)));
		formula.add(mkClause(new Literal(3, true), new Literal(1, false)));
		formula.add(mkClause(new Literal(4, false), new Literal(1, false), new Literal(5, false), new Literal(6, false)));
		formula.add(mkClause(new Literal(3, true), new Literal(1, false), new Literal(5, false), new Literal(6, true)));
		formula.add(mkClause(new Literal(7, false), new Literal(8, true), new Literal(9, false), new Literal(8, false)));
		final Map<Integer, Boolean> assignments = new HashMap<>();
		final DPLLSolver solver = new DPLLSolver(10);
		boolean isSatisfiable = solver.solve(formula, assignments);
		assertEquals("{0=true, 1=true, 2=true, 3=false, 4=true, 5=true, 7=true, 9=true} sattrue", assignments+" sat"+isSatisfiable);
	}
	
	@Test(timeout = 500)
	public void custTest__chooseVariable__tricky() {
		final Map<Integer, Boolean> assignments = new HashMap<>();
		assignments.put(0, false);
		assignments.put(1, false);
		assignments.put(3, false);
		assertEquals(DPLLSolverPublicTest.EX_NAME_chooseVariable + "() should return 2", 2, new DPLLSolver(3).chooseVariable(assignments));
	}
	
	@Test(timeout = 1000)
	public void custTest__assignPureLiterals() {
		// (!v0 | v1) & (!v1 | !v2) & (v1 | v2) ->  (!v1 | !v2) & (v1 | v2)
		// (!v0 | v0) & (!v1 | !v2) & (!v1 | v2) ->   (!v0 | v0)
		final List<List<Literal>> formula = new LinkedList<>();
		formula.add(mkClause(new Literal(0, true), new Literal(0, false)));
		formula.add(mkClause(new Literal(1, true), new Literal(2, true)));
		formula.add(mkClause(new Literal(1, true), new Literal(2, false)));
		final Map<Integer, Boolean> assignments = new HashMap<>();
		final DPLLSolver solver = new DPLLSolver(3);
		solver.assignPureLiterals(formula, assignments);
		//System.out.println("form"+formula+" ass:"+assignments);
		assertEquals("failed", "form[[\u00ACv0, v0]] ass:{1=false}", "form"+formula+" ass:"+assignments);
	}
	
	@Test(timeout = 1000)
	public void custTest__propagateUnitClauses__emptyKlausel() {
		// (v0) & (v1 | !v2) & (!v1 | v2) ==> (v1 | !v2) & (!v1 | v2)
		final List<List<Literal>> formula = new LinkedList<>();
		formula.add(new LinkedList<Literal>());
		final Map<Integer, Boolean> assignments = new HashMap<>();
		final Solver solver = new DPLLSolverCountingMock(3);
		solver.propagateUnitClauses(formula, assignments);
		//System.out.println("form"+formula+" ass:"+assignments);
		assertEquals("failed", "form[[]] ass:{}", "form"+formula+" ass:"+assignments);

	}
	@Test(timeout = 1000)
	public void custTest__propagateUnitClauses__multiRemoval() {
		// (v0) & (v1 | !v2) & (!v1 | v2) ==> (v1 | !v2) & (!v1 | v2)
		final List<List<Literal>> formula = new LinkedList<>();
		formula.add(mkClause(new Literal(1, true)));
		formula.add(mkClause(new Literal(1, false), new Literal(4, true)));
		formula.add(mkClause(new Literal(2, false)));
		formula.add(mkClause(new Literal(3, false)));
		final Map<Integer, Boolean> assignments = new HashMap<>();
		final Solver solver = new DPLLSolverCountingMock(3);
		solver.propagateUnitClauses(formula, assignments);
		//System.out.println("form2"+formula+" ass:"+assignments);
		assertEquals("failed", "form2[[\u00ACv4]] ass:{1=false, 2=true, 3=true}", "form2"+formula+" ass:"+assignments);
	}
	@Test(timeout = 1000)
	public void custTest__propagateUnitClauses__multiRemoval2() {
		// (!v1) & (v1 | v2) & (v2) & (v3) => 
		final List<List<Literal>> formula = new LinkedList<>();
		formula.add(mkClause(new Literal(1, true)));
		formula.add(mkClause(new Literal(2, false)));
		formula.add(mkClause(new Literal(1, false), new Literal(2, false)));
		formula.add(mkClause(new Literal(3, false)));
		final Map<Integer, Boolean> assignments = new HashMap<>();
		final Solver solver = new DPLLSolverCountingMock(3);
		solver.propagateUnitClauses(formula, assignments);
		//System.out.println("form1"+formula+" ass:"+assignments);
		assertEquals("failed", "form1[] ass:{1=false, 2=true, 3=true}", "form1"+formula+" ass:"+assignments);
	}
````